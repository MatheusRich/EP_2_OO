PASTAS E ARQUIVOS
=================

docs: Local onde estão armazenadas as imagens a serem decifradas.

EP_2_OO: Local onde estão armazenadas as classes, interfaces e demais itens do programa.

MR Photo Editor.jar: Atalho para a execução do programa.




COMO COMPILAR E EXECUTAR
========================

- Não é necessário compilar o programa, apenas clique no atalho "MR Photo Editor" disponível e ele executará.

- Caso deseje, pode-se abrir o projeto com uma IDE como o NetBeans, por exemplo.




UTILIZANDO O PROGRAMA
=====================

ABRINDO UM ARQUIVO
------------------

- O programa abrirá na sua tela inicial. O usuário tem acesso à algumas abas de ajuda no menu Help e algumas informações sobre o desenvolvedor na aba About.

- Para começar a edição utilize a aba File e em seguida o botão Open. Escolha o arquivo desejado. Só arquivos .ppm e .pgm serão permitidos, além isto o programa apresentará uma mensagem de erro.

EDITANDO UM ARQUIVO
-------------------

- Após abrir o usuário terá diferentes opões de acordo com o formato do arquivo aberto:

**PGM**

- Para imagens do tipo .pgm o usuário terá disponíveis os filtros Negative, Smooth e Sharpen. Para adicioná-los basta clicar na aba Edit e escolher um filtro.

- Em imagens .pgm há disponível, também, a ferramenta Hidden Message Finder, disponível na aba Tools. Esta ferramenta desvenda uma mensagem escondida na imagem e apresenta-a ao usuário.

**PPM**

- Para imagens do tipo .pgm o usuário terá disponíveis os filtros Red, Green e Blue. Para adicioná-los basta clicar na aba Edit e escolher um filtro.

SALVANDO UM ARQUIVO
-------------------

- Após editar um arquivo o usuário pode salvá-lo clicando no menu File, e em seguida no botão Save. 

- Em seguida escolhe-se o local em que se deseja salvar o arquivo, bem como seu nome. Por fim, clica-se no botão Save.


OBSERVAÇÕES
===========

- O programa está inteiramente em inglês.

- Algumas imagens podem demorar a carregar.

- A imagem Lena.pgm está, como visto no exercício de programação anterior, com problemas para desvendar a mensagem secreta, devido aos caracteres especiais contidos na mensagem. Visto isso foi adicionado uma outra imagem chamada modifiedLena.pgm na qual a posição inical da mensagem foi alterada, para que o usuário possa ver, de forma resumida, o conteúdo da mensagem secreta.