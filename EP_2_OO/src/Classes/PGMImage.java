package Classes;


import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author matheus_richard
 */
public class PGMImage extends Image
{
    private int pixel;
    private int extractedBit;
    private int extractedByte;
    
    public PGMImage()
    {
        
    }
     
    public byte[] openPGMImage(String path) throws FileNotFoundException, IOException 
        {   

        FileInputStream image = new  FileInputStream(path);
        FileInputStream imagePixels = new FileInputStream(path);
        Scanner reader = new Scanner(image);
        
        String tempComment = new String();
        
        this.setMagicNumber(reader.next());
        tempComment += reader.next();
        this.setInitialPosition(reader.nextInt());        
        tempComment += " " + this.getInitialPosition() + " ";
        tempComment += reader.next() + " ";
        tempComment += reader.next() + " ";
        tempComment += reader.next() + " ";
        tempComment += reader.next() + " ";
        tempComment += reader.next();
        this.setComment(tempComment);
        
        if(reader.hasNext()  && reader.hasNextInt())
        {   
            this.setWidth(reader.nextInt());
        }
        
        if(reader.hasNext()  && reader.hasNextInt())
        {   
            this.setHeight(reader.nextInt());
        }
        if (reader.hasNext()  && reader.hasNextInt())
        {
            this.setMaximumPixel(reader.nextInt());
        }
        
        else
        {
            System.out.println("ERROR: CORRUPTED IMAGE");
        }
        
    
        String fullHeader = new String();
        
        fullHeader+= (getMagicNumber()+"\n"+getComment()+"\n"+getWidth()+" "+getHeight()+"\n"+getMaximumPixel()+"\n");
        setHeader(fullHeader);
        imagePixels.skip(fullHeader.length());
        
        byte[] PGMBytes = new byte[height*width];
        int count=0;
        
        while(count<height*width)
        {
            PGMBytes[count] = (byte)imagePixels.read();
            count++;
        }
        
        setPixels(PGMBytes);
        return PGMBytes;
    }

    public String decipherImage() throws FileNotFoundException, IOException
    {
        int count=1;
        String message = new String();

        do
        {
            pixel = ((int)(pixels[initialPosition+count]) & 0x01);
            extractedBit = pixel;
            if(count%8!=0)
            {
                extractedByte = (int)((extractedByte<<1) | pixel);
            }
            else
            {
                extractedByte = (int)(extractedByte | pixel);
                message += (char)extractedByte;
		extractedByte= 0;
            }
            
            count++;
        }while((char)extractedByte != '#');
        
        return message;
        
    }
    
    public void negativeFilter() throws IOException
    {

            byte[] copy = new byte[height*width];
            
            for(int i =0; i<height*width;i++)
            {
                copy[i] = (byte) (maximumPixel-pixels[i]);
            }            
        
            setPixels(copy);
    }
    
    public void smoothFilter() throws FileNotFoundException, IOException
    {
        int filtro[]={1, 1, 1, 1, 1, 1, 1, 1, 1};
        int value;
        BufferedImage imagem_pgm = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        byte[] pixels3 = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
        char[] pixels = new char[getHeight()*getWidth()];
        int contador=0;
        
        
            for(int contador1=0; contador1<3; contador1++){
		for(int contador2=0; contador2<getWidth(); contador2++){
                        pixels3[contador1+contador2*getWidth()] = getPixels()[contador1+contador2*getWidth()];
		}
	}
	
	for(int contador1=0; contador1<3; contador1++){
		for(int contador2=0; contador2<getHeight(); contador2++){
                        pixels3[contador1+contador2*getWidth()] = getPixels()[contador1+contador2*getWidth()];
		}
	}

	for(int contador1=3/2; contador1<getWidth()-3/2; contador1++){
		for(int contador2=3/2; contador2<getHeight()-3/2; contador2++){
			value = 0;
			for(int contador3=-3/2; contador3<=3/2; contador3++){
				for(int contador4=-3/2; contador4<=3/2;contador4++){
					value += filtro[(contador3+1)+3*(contador4+1)]*(pixels[(contador1+contador3)+(contador4+contador2)*getWidth()]);
				}
			}
				
				value/=9;
				value = value < 0 ? 0 : value;
				value = value > getMaximumPixel()? getMaximumPixel(): value;
                                pixels3[contador1+contador2*getWidth()] = (byte) value;
		}
	}
        
            setPixels(pixels3);
            System.out.println("H W NM "+getHeight()+" "+getWidth()+" "+getMaximumPixel());
        }
    

    

    public byte[] getPixels()
    {
        return pixels;
    }
    
    
    public void setPixels(byte [] pixels)
    {
        this.pixels = new byte[this.getHeight()*this.getWidth()];
        this.pixels = pixels;
    }
}
