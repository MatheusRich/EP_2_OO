package Classes;

import Graphics.FileNotOpenScreen;
import Graphics.InitialScreen;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFileChooser;

/**
 *
 * @author matheus_richard
 */
public class PPMImage extends Image
{    
    private byte pixel;
    private int choosenFilter;
    private byte bytes[];

    public PPMImage()
    {
        
    }
    

    public byte[] openPPMImage(String path) throws FileNotFoundException, IOException
    {
        FileInputStream image = new  FileInputStream(path);
        FileInputStream imagePixels = new FileInputStream(path);
        Scanner reader = new Scanner(image);
        
        this.setMagicNumber(reader.next());
        setComment(reader.next()); 
        
        if(reader.hasNext()  && reader.hasNextInt())
        {   
            this.setWidth(reader.nextInt());
        }
        
        if(reader.hasNext()  && reader.hasNextInt())
        {   
            this.setHeight(reader.nextInt());
        }
        if (reader.hasNext()  && reader.hasNextInt())
        {
            this.setMaximumPixel(reader.nextInt());
        }
        
        else
        {
            System.out.println("ERROR: CORRUPTED IMAGE");
        }
        
    
        String fullHeader = new String();
        byte[] pixels2 = new byte[height*width*3];
        
        fullHeader+= (getMagicNumber()+"\n"+getComment()+"\n"+getWidth()+" "+getHeight()+"\n"+getMaximumPixel()+"\n");
        setHeader(fullHeader);
        imagePixels.skip(fullHeader.length());
        
        int count=0;
        while(count<height*width*3)
        {
            pixels2[count] =(byte) imagePixels.read();
            count++;
        }
        setBytes(pixels2);

        return pixels2;
    }
    
    public byte[] RGBFilters(int choosenFilter) throws FileNotFoundException, IOException
    {
        this.choosenFilter = choosenFilter;
        
        if(bytes != null)
        {
                int count=0;
                byte[] copy = new byte[height*width*3];

                while(count<(height*width*3))
                {
                    for(int j=1;j<=3;j++)
                    {
                        if(j!=this.choosenFilter)
                        {
                            copy[count]=0;
                        }
                        else
                        {
                            copy[count]=(bytes[count]);
                        }

                        count++;	
                    }
                    
                }
            
            setBytes(copy);
        }
        else
        {   
            System.out.println("ERROR: OPEN THE IMAGE FIRST.");
        }
        return bytes;
    }
    
    public byte[] getBytes() 
    {
        return bytes;
    }
    
    public void setBytes(byte[] bytes) 
    {
        this.bytes = bytes;
    }
    
}
