    package Classes;

import Graphics.InitialScreen;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JDesktopPane;

/**
 *
 * @author matheus_richard
 */
public class Image
{
    protected String fileLocation;
    protected String magicNumber;
    protected String comment;
    protected int height;
    protected int width;
    protected int maximumPixel;
    protected String header;
    protected byte [] pixels;
    protected int initialPosition;

    
    public Image()
    {
        
    }
    
    @SuppressWarnings("ConvertToStringSwitch")
    public Image openImage(String filePath) throws IOException,IllegalArgumentException,FileNotFoundException
    { 
            setFileLocation(filePath);
            FileInputStream image = new FileInputStream(filePath);
            Scanner reader = new Scanner(image);
            
            if(reader.hasNext())
            {
                setMagicNumber(reader.next());
            
                if(getMagicNumber().equals("P5"))
                {                   
                    PGMImage pgmImage = new PGMImage();
                    setPixels(pgmImage.openPGMImage(filePath)); //Overrided method.
                    setHeader(pgmImage.getHeader());
                    setPixels(pgmImage.getPixels());
                    setHeight(pgmImage.getHeight());
                    setWidth(pgmImage.getWidth()); 
                    setInitialPosition(pgmImage.getInitialPosition());
                    setMaximumPixel(pgmImage.getMaximumPixel());
                    
                }
                else if(getMagicNumber().equals("P6"))
                {                
                    PPMImage ppmImage = new PPMImage();
                    setPixels(ppmImage.openPPMImage(filePath));
                    setHeader(ppmImage.getHeader());
                    setPixels(ppmImage.getBytes());
                    setHeight(ppmImage.getHeight());
                    setWidth(ppmImage.getWidth());
                }
                else
                {
                    throw new IllegalArgumentException("File Type Unknown");
                }            
            
            }
            else
            {
                    pixels =null;
                    throw new IllegalArgumentException("File Type Unknown");
            }
            
       return this;    
    }

    public byte[] getPixels() {
        return pixels;
    }

    public void setPixels(byte[] pixels) {
        this.pixels = pixels;
    }
    
    public String getFileLocation()
    {
        return fileLocation;
    }
    public String getMagicNumber()
    {
        return magicNumber;
    }
    
    public String getComment()
    {
        return comment;
    }
    
    public int getHeight()
    {
        return height;
    }
    
    public int getWidth()
    {
        return width;
    }
    
    public int getMaximumPixel()
    {
        return maximumPixel;
    }
    
    public void setFileLocation(String fileLocation)
    {
        this.fileLocation = fileLocation;
    }
    
    public void setMagicNumber(String magicNumber)
    {
        this.magicNumber = magicNumber;
    }
    
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }
    
    public void setMaximumPixel(int maximumPixel)
    {
        this.maximumPixel = maximumPixel;
    }
   
    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
    
    public int getInitialPosition()
    {
        return initialPosition;
    }

    
    public void setInitialPosition(int initalPosition)
    {
        this.initialPosition = initalPosition;
    }

}
